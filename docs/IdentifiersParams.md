# FinApiResTfulServices.IdentifiersParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ids** | **[Number]** | List of identifiers | 


